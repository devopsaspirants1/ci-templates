stages:
  - qa

variables:
  DEPENDENCY_SCANNING_REPORT: gl-dependency-scanning-report.json

dependency_scanning:
  variables:
    GEMNASIUM_DB_REF_NAME: "v1.2.142"
    RETIREJS_JS_ADVISORY_DB: "jsrepository.json"
    RETIREJS_NODE_ADVISORY_DB: "npmrepository.json"
    BUNDLER_AUDIT_ADVISORY_DB_REF_NAME: "736ff80b04ce6621a44def4ae3c9f889c5ea7fe4"
  artifacts:
    paths: 
      - gl-dependency-scanning-report.json

.qa-dependency_scanning:
  stage: qa
  image: alpine:3.9
  variables:
    WANT: $DEPENDENCY_SCANNING_REPORT
    GOT: $DEPENDENCY_SCANNING_REPORT
  script:
    - apk update && apk add jq
    - echo "Please trigger update-$CI_JOB_NAME job to create a MR if this job fails and you believe the expected report is wrong."
    - wget https://gitlab.com/gitlab-org/security-products/ci-templates/-/raw/master/scripts/compare_reports.sh
    - |
      if [[ -n "$DS_REPORT_URL" ]]; then
        echo "Fetching $DS_REPORT_URL"
        mkdir -p qa/expect
        wget -O "qa/expect/$WANT" "$DS_REPORT_URL"
      fi
    - sh ./compare_reports.sh ds $GOT qa/expect/$WANT

qa-gemnasium-dependency_scanning:
  needs: ["gemnasium-dependency_scanning"]
  extends: .qa-dependency_scanning
  # same rules as scanning job
  rules:
    - if: $DEPENDENCY_SCANNING_DISABLED
      when: never
    - if: $CI_COMMIT_BRANCH &&
          $GITLAB_FEATURES =~ /\bdependency_scanning\b/ &&
          $DS_DEFAULT_ANALYZERS =~ /gemnasium([^-]|$)/
      exists:
        - '{Gemfile.lock,*/Gemfile.lock,*/*/Gemfile.lock}'
        - '{composer.lock,*/composer.lock,*/*/composer.lock}'
        - '{gems.locked,*/gems.locked,*/*/gems.locked}'
        - '{go.sum,*/go.sum,*/*/go.sum}'
        - '{npm-shrinkwrap.json,*/npm-shrinkwrap.json,*/*/npm-shrinkwrap.json}'
        - '{package-lock.json,*/package-lock.json,*/*/package-lock.json}'
        - '{yarn.lock,*/yarn.lock,*/*/yarn.lock}'
        - '{packages.lock.json,*/packages.lock.json,*/*/packages.lock.json}'
        - '{conan.lock,*/conan.lock,*/*/conan.lock}'

qa-gemnasium-maven-dependency_scanning:
  needs: ["gemnasium-maven-dependency_scanning"]
  extends: .qa-dependency_scanning
  # same rules as scanning job
  rules:
    - if: $DEPENDENCY_SCANNING_DISABLED
      when: never
    - if: $CI_COMMIT_BRANCH &&
          $GITLAB_FEATURES =~ /\bdependency_scanning\b/ &&
          $DS_DEFAULT_ANALYZERS =~ /gemnasium-maven/
      exists:
        - '{build.gradle,*/build.gradle,*/*/build.gradle}'
        - '{build.gradle.kts,*/build.gradle.kts,*/*/build.gradle.kts}'
        - '{build.sbt,*/build.sbt,*/*/build.sbt}'
        - '{pom.xml,*/pom.xml,*/*/pom.xml}'

qa-gemnasium-python-dependency_scanning:
  needs: ["gemnasium-python-dependency_scanning"]
  extends: .qa-dependency_scanning
  # same rules as scanning job
  rules:
    - if: $DEPENDENCY_SCANNING_DISABLED
      when: never
    - if: $CI_COMMIT_BRANCH &&
          $GITLAB_FEATURES =~ /\bdependency_scanning\b/ &&
          $DS_DEFAULT_ANALYZERS =~ /gemnasium-python/
      exists:
        - '{requirements.txt,*/requirements.txt,*/*/requirements.txt}'
        - '{requirements.pip,*/requirements.pip,*/*/requirements.pip}'
        - '{Pipfile,*/Pipfile,*/*/Pipfile}'
        - '{requires.txt,*/requires.txt,*/*/requires.txt}'
        - '{setup.py,*/setup.py,*/*/setup.py}'
        # Support passing of $PIP_REQUIREMENTS_FILE
        # See https://docs.gitlab.com/ee/user/application_security/dependency_scanning/#configuring-specific-analyzers-used-by-dependency-scanning
    - if: $CI_COMMIT_BRANCH &&
          $GITLAB_FEATURES =~ /\bdependency_scanning\b/ &&
          $DS_DEFAULT_ANALYZERS =~ /gemnasium-python/ &&
          $PIP_REQUIREMENTS_FILE

qa-bundler-audit-dependency_scanning:
  needs: ["bundler-audit-dependency_scanning"]
  extends: .qa-dependency_scanning
  # same rules as scanning job
  rules:
    - if: $DEPENDENCY_SCANNING_DISABLED
      when: never
    - if: $CI_COMMIT_BRANCH &&
          $GITLAB_FEATURES =~ /\bdependency_scanning\b/ &&
          $DS_DEFAULT_ANALYZERS =~ /bundler-audit/
      exists:
        - '{Gemfile.lock,*/Gemfile.lock,*/*/Gemfile.lock}'

qa-retire-js-dependency_scanning:
  needs: ["retire-js-dependency_scanning"]
  extends: .qa-dependency_scanning
  # same rules as scanning job
  rules:
    - if: $DEPENDENCY_SCANNING_DISABLED
      when: never
    - if: $CI_COMMIT_BRANCH &&
          $GITLAB_FEATURES =~ /\bdependency_scanning\b/ &&
          $DS_DEFAULT_ANALYZERS =~ /retire.js/
      exists:
        - '{package.json,*/package.json,*/*/package.json}'

.update-qa-dependency_scanning:
  stage: qa
  image: alpine
  when: manual
  except:
    - master
  variables:
    WANT: $DEPENDENCY_SCANNING_REPORT
    GOT: $DEPENDENCY_SCANNING_REPORT
  script:
    - apk add --no-cache git
    - cp ./$GOT ./qa/expect/$WANT
    - git config --global user.email "gitlab-bot@gitlab.com"
    - git config --global user.name "GitLab Bot"
    - git config --global credential.username "gitlab-bot"
    - export BRANCH="$CI_JOB_NAME-$CI_PIPELINE_IID"
    - git checkout -b $BRANCH
    - git add ./qa/expect/$WANT
    - git commit -m "Update expectations for $JOB job"
    - git push -o merge_request.create -o merge_request.remove_source_branch -o merge_request.target=$CI_COMMIT_REF_NAME ${CI_PROJECT_URL/https:\/\/gitlab.com/https://gitlab-bot:$GITLAB_TOKEN@gitlab.com}.git $BRANCH
